


describe("Tickets", () => {

    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'));

    it("preencher os campos de texto", () => {
        const primeiro = "Ermeson";
        const segundo = "Nóbrega";


        cy.get('#first-name').type(primeiro);
        cy.get("#last-name").type(segundo);
        cy.get("#email").type("ermeson@gmail.com");
        cy.get("#requests").type("teste");
        cy.get("#signature").type(`${primeiro} ${segundo}`);


    });

    it("seleção de ", () => {
        
        cy.get('#ticket-quantity').select('2');

    });

    it("seleção de radio button", () => {

        cy.get('#vip').check();
    });

    it("seleção de checkboxes", () => {

        cy.get('#friend').check();
        cy.get('#social-media').check();
        cy.get('#friend').uncheck();


    });





    it("verificar nome do TicketBox", () => {

         cy.get('h1').should("contain", "TICKETBOX");


    });

    it("verificar invalid e-mail", () => {

        cy.get('#email').type("email-gmail.com");
        cy.get('#email.invalid').should('exist');
        cy.get('#email').clear();



   });


   it("todos os campos", () => {

    const primeiro = "Ermeson";
    const segundo = "Nóbrega";
    const name = `${primeiro} ${segundo}`;


    cy.get('#first-name').type(primeiro);
    cy.get("#last-name").type(segundo);
    cy.get("#email").type("ermeson@gmail.com");

    cy.get('#ticket-quantity').select('2');
    cy.get('#vip').check();

    cy.get('#friend').check();
    cy.get("#requests").type("teste");

    cy.get(".agreement p").should(
        "contain",
        `I, ${name}, wish to buy 2 VIP tickets. I understand that all ticket sales are final.`
    );

    cy.get('#agree').check();
    assert(cy.get('.active').should('be.enabled'));
    cy.get('.reset').click();
    assert(cy.get('[type="submit"]').should('be.disabled'));

});

});

//Término